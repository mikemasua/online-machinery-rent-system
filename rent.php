<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<?php
session_start();
require("db.php");

?>
<html>
<head>
<title>Lease Your Chosen Tool</title>
<link href="css/styles.css" rel="stylesheet" type="text/css" />
<style>
h1 { position: relative;margin-top: 20px;}
h1.one {margin-top: 0;}
h1.one:before {content: "";display: block;border-top: solid 1px black;width: 100%;height: 1px;position: absolute;top: 50%;z-index: 1;}
h1.one span {background: #ffc;padding: 0 20px; position: relative;z-index: 5;}
</style>
<script>
function clearform()
{
document.getElementById("name1").value=""; //don't forget to set the textbox id
document.getElementById("name2").value="";
document.getElementById("gen").value="";
document.getElementById("mobile").value="";
document.getElementById("mail").value="";
	  ;
}
</script>
<!--sa calendar-->
<script type="text/javascript" src="js/datepicker.js"></script>
<link href="css/demo.css" rel="stylesheet" type="text/css" />
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
//<![CDATA[

/*
        A "Reservation Date" example using two datePickers
        --------------------------------------------------

        * Functionality

        1. When the page loads:
                - We clear the value of the two inputs (to clear any values cached by the browser)
                - We set an "onchange" event handler on the startDate input to call the setReservationDates function
        2. When a start date is selected
                - We set the low range of the endDate datePicker to be the start date the user has just selected
                - If the endDate input already has a date stipulated and the date falls before the new start date then we clear the input's value

        * Caveats (aren't there always)

        - This demo has been written for dates that have NOT been split across three inputs

*/

function makeTwoChars(inp) {
        return String(inp).length < 2 ? "0" + inp : inp;
}

function initialiseInputs() {
        // Clear any old values from the inputs (that might be cached by the browser after a page reload)
        document.getElementById("sd").value = "";
        document.getElementById("ed").value = "";

        // Add the onchange event handler to the start date input
        datePickerController.addEvent(document.getElementById("sd"), "change", setReservationDates);
}

var initAttempts = 0;

function setReservationDates(e) {
        // Internet Explorer will not have created the datePickers yet so we poll the datePickerController Object using a setTimeout
        // until they become available (a maximum of ten times in case something has gone horribly wrong)

        try {
                var sd = datePickerController.getDatePicker("sd");
                var ed = datePickerController.getDatePicker("ed");
        } catch (err) {
                if(initAttempts++ < 10) setTimeout("setReservationDates()", 50);
                return;
        }

        // Check the value of the input is a date of the correct format
        var dt = datePickerController.dateFormat(this.value, sd.format.charAt(0) == "m");

        // If the input's value cannot be parsed as a valid date then return
        if(dt == 0) return;

        // At this stage we have a valid YYYYMMDD date

        // Grab the value set within the endDate input and parse it using the dateFormat method
        // N.B: The second parameter to the dateFormat function, if TRUE, tells the function to favour the m-d-y date format
        var edv = datePickerController.dateFormat(document.getElementById("ed").value, ed.format.charAt(0) == "m");

        // Set the low range of the second datePicker to be the date parsed from the first
        ed.setRangeLow( dt );
        
        // If theres a value already present within the end date input and it's smaller than the start date
        // then clear the end date value
        if(edv < dt) {
                document.getElementById("ed").value = "";
        }
}

function removeInputEvents() {
        // Remove the onchange event handler set within the function initialiseInputs
        datePickerController.removeEvent(document.getElementById("sd"), "change", setReservationDates);
}

datePickerController.addEvent(window, 'load', initialiseInputs);
datePickerController.addEvent(window, 'unload', removeInputEvents);

//]]>
</script>
</head>
<body bgcolor="#F4FFE4">
<div id="hady"><h1 style="background: #2F4F4F; color:#4CAF50; margin-bottom:10px; font-family:Baskerville, 'Palatino Linotype', Palatino, 'Century Schoolbook L', 'Times New Roman', serif;" align="center"> <font size="+2">Online Machinery Rentals </font></h1>
</div>
<div id="logos">
</div><br /><br />
<style>#link ul{margin-left:400px;}</style>
<div id="link">
<ul>
<li><a href="userss.php">Rent Equipment</a></li>
<li><a href="#">Rent</a></li>
<li><a href="code.php">Print Receipt</a></li>
</ul>
</div>
<h1 style="background: #2F4F4F; color:#4CAF50; font-family:Baskerville, 'Palatino Linotype', Palatino, 'Century Schoolbook L', 'Times New Roman', serif; margin-top:5px; " align="center"> <font size="+3">Rent your Tool |
 </font><a href="logout.php"><font style="color:#FFFFFF; text-decoration:none; size:10% margin-left:100%; ">Logout</font></a></h1>
<!--<a href="userss.php">Print</a> Your receipt-->
<div id = "form" style ="width:1000px; height:500px" align="center" >
<form action="" method="post">	
<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once('db.php');
$id = $_GET['idy'];
 if(isset($_GET['idy'])){


$result = mysql_query("SELECT * FROM users WHERE user_id='$id'");
$row = mysql_fetch_array($result)?>
    
	
<h1 class="one"><span><font face="grey">Personal Information</font></span></h1>
<label style="margin-left: -650px;">Names:</label></br></br>
<input type="text" name="fname" value="<?php echo $row['fname']; ?>" placeholder="First Name/ Usename" id="name1" style="width: 150px; margin-left: -100px; height: 25px;"/>*Edit
<input type="text" name="idno" value="<?php echo $row['idno']; ?>" placeholder="ID Number" id="idno" style="width: 150px; margin-left: 50px; height: 25px;"/>*Edit
<input type="text" name="lname" value="<?php echo $row['lname']; ?>" placeholder="Last name" id="name2" style="width: 150px; margin-left: 50px; height: 25px;"/>*Edit
<input type="text" value="<?php echo $row['gender']; ?>" name='gender' id="gen" placeholder="Gender" style="width: 150px; margin-left: 50px; height: 25px;" />
			
<h1 class="one"><span>Contacts, Address & Equipment</span></h1>
<label style="margin-right: 0px;">Equipment:</label>
<label style="margin-left: -650px;">Mobile & E-mail Address:</label></br> </br></br>

<input type="text" name="tel" value="<?php echo $row['phone']; ?>" placeholder="Mobile No" id="mobile" style="width: 150px; margin-left: -50px; height: 25px;"/>*Edit
<input type="text" name="email" value="<?php echo $row['email']; ?>" placeholder="Email Address" id="mail"style="width: 150px; margin-left: 50px; height: 25px;"/>*Edit

<?php
 
 }
 ?>
 <?php
//include_once('db.php');
 $id = $_GET['idy'];
 if(isset($_GET['idy']))
{
	$results = mysql_query("SELECT * FROM equipments WHERE equip_id='$id'");
	$row = mysql_fetch_array($results)?>
	<input type="hidden" name="cost1" value="<?php echo $row['cost']; ?>" />
<input type="text" name="category" value="<?php echo $row['category']; ?>"placeholder="Category" id="category"style="width: 150px; margin-left: 100px; height: 25px;"/></br></br>
<input type="text" name="equipment" value="<?php echo $row['eqname']; ?>" placeholder="Equipment" id="equipment"style="width: 150px; margin-left: 480px; height: 25px;"/></br></br>
<label style="margin-left: 380px;">Reg N0.</label><br><br>
<input type="text" name="regno" value="<?php echo $row['regno']; ?>"placeholder="Reg No" id="regno"style="width: 150px; margin-left: 480px; height: 25px;"/></br></br>



<?php
 }
 ?>
 <label style="margin-left: -180px;">When Do You Want To Collect Equipment?:</label>
<input type="text" name="start" style="width: 150px; margin-left: 100px; height: 25px;" placeholder="Select Date"  class="w8em format-y-m-d highlight-days-67 range-low-today"  id="sd" value="" maxlength="10" /><br><br>
<label style="margin-left: -170px;">For How Long Do You Want To Rent Equipment?:</label>
<input type="text" name="days" required="required" placeholder="No Of Days" id="category"style="width: 150px; margin-left: 50px; height: 25px;"/></br></br>
<input type="submit" name="save"   value="RENT" style="height:40px;"/>
</form>
</div>
</body>

</html>


<?php

$id = $_GET['idy'];
 if(isset($_GET['idy']))
{
	

if(!empty($_POST['fname']) && !empty($_POST['lname']) && !empty($_POST['gender']) && !empty($_POST['tel'])
 && !empty($_POST['email']) && !empty($_POST['category']) && !empty($_POST['regno']) && !empty($_POST['equipment']))
{
    $firname = mysql_real_escape_string($_POST['fname']);
    $lastname = mysql_real_escape_string($_POST['lname']); 
	$idno = mysql_real_escape_string($_POST['idno']);
	$gen = mysql_real_escape_string($_POST['gender']);
	$tele = mysql_real_escape_string($_POST['tel']);
	$mail = mysql_real_escape_string($_POST['email']);
	$cat = mysql_real_escape_string($_POST['category']);
	$regno = mysql_real_escape_string($_POST['regno']);
	$equipment = mysql_real_escape_string($_POST['equipment']);
	$date = mysql_real_escape_string($_POST['start']);
	$days = mysql_real_escape_string($_POST['days']);
	$cost= mysql_real_escape_string($_POST['cost1']);
	$total = $days*$cost;
	
$registerquery = mysql_query("INSERT INTO booking (fname, lname,idno, gender, phone, email, category, equipment,regno,dates,days, total) VALUES('".$firname."', '".$lastname."','".$idno."','".$gen."','".$tele."','".$mail."','".$cat."','".$equipment."','".$regno."','".$date."','".$days."','".$total."')");
if($registerquery)
{ 
$sqld = mysql_query("DELETE FROM equipments WHERE equip_id='$id'");
echo '<script type="text/javascript">alert("Success. Your order send successfully.! !");</script>';
echo "<script>window.open('userss.php','_self')</script>";
}   
}
}
?>