-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2017 at 11:04 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `equipment`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(2, 'masua', 'masua');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `book_id` int(10) NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `idno` int(10) NOT NULL,
  `gender` text NOT NULL,
  `phone` int(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `category` text NOT NULL,
  `equipment` text NOT NULL,
  `regno` varchar(10) NOT NULL,
  `dates` varchar(12) NOT NULL,
  `days` varchar(10) NOT NULL,
  `total` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`book_id`, `fname`, `lname`, `idno`, `gender`, `phone`, `email`, `category`, `equipment`, `regno`, `dates`, `days`, `total`) VALUES
(1, 'Fredrick', 'Toroitich', 29655666, 'Male', 729350778, 'kogeifred@gmail.com', 'Planting', 'broadcaster seeder', 'BRS122', '2016/02/24', '8', 9600),
(2, 'David ', 'Kariuki', 25665874, 'Male', 729358669, 'kolop@home.pk', 'Planting', 'planter', 'PLN231', '2016/02/23', '4', 6000),
(3, 'myk', 'myk@gmail.com', 31547960, 'male', 900003677, 'mykdemasua@gmail.com', 'Transport systems', 'Ford 5000', 'KAM 112', '', '5', 22500),
(4, 'Katty', 'Parry', 31547980, 'Female', 765378936, 'kattyp2222@gmx.com', 'Cement mixing', 'mixer', 'KAK 1335K', '', '8', 31272);

-- --------------------------------------------------------

--
-- Table structure for table `equipments`
--

CREATE TABLE `equipments` (
  `equip_id` int(10) NOT NULL,
  `category` text NOT NULL,
  `eqname` varchar(25) NOT NULL,
  `regno` varchar(10) NOT NULL,
  `cost` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipments`
--

INSERT INTO `equipments` (`equip_id`, `category`, `eqname`, `regno`, `cost`) VALUES
(1, 'Cement mixing', 'Massey Fergusson 365', 'KAN 345J', 5000),
(3, 'Excavating', 'planter', 'PLN231', 1200),
(5, 'Drilling', 'Ford 6000', 'KAS 455', 4200),
(6, 'Lifting ', 'jembe', '800Q0Q', 3000),
(7, 'Fertilizing & Pest control', 'Loader 1335', 'KBL 129L', 2000),
(10, 'Excavating', 'Excavator12', 'KBL 129H', 2333),
(11, 'Compaction', 'Sand Blaster', 'KBP 898L', 10000),
(12, 'Compaction', 'Sand Truck', 'KBA 678U', 12000);

-- --------------------------------------------------------

--
-- Table structure for table `farmers`
--

CREATE TABLE `farmers` (
  `user_id` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `idno` int(10) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone` int(10) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `farmers`
--

INSERT INTO `farmers` (`user_id`, `fname`, `lname`, `idno`, `gender`, `phone`, `email`, `password`) VALUES
(3, 'Joash', 'misaro', 22554765, 'Male', 707664012, 'kog65eifred@gmail.co', '3215'),
(5, 'joan', 'mwende', 66987452, 'Female', 707664012, 'josh@yahoo.com', 'joan'),
(7, 'peter', 'james', 315478940, 'Male', 728980269, 'admin@admin.com', 'adminooooee'),
(9, 'Katty', 'Parry', 31547980, 'Female', 765378936, 'kattyp2222@gmx.com', 'kattyparryyyy392992'),
(10, 'patty', 'Erickson', 31547982, 'Male', 765378935, 'pattyyeyeyeee@gmx.co', 'pattyyyyyereoov'),
(11, 'joroge', 'kinutha', 45637722, 'Male', 702087965, 'marviskanyi@gmail.co', 'munene');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `equipments`
--
ALTER TABLE `equipments`
  ADD PRIMARY KEY (`equip_id`);

--
-- Indexes for table `farmers`
--
ALTER TABLE `farmers`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `book_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `equipments`
--
ALTER TABLE `equipments`
  MODIFY `equip_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `farmers`
--
ALTER TABLE `farmers`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
