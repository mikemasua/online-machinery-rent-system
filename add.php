<!doctype html>
<?php
include_once("db.php");
?>
<html>
<head>
<title>Add Equipment</title>
<link href="css/styles.css" rel="stylesheet" type="text/css">
<style>
h1 {position: relative;margin-top: 20px;}
h1.one { margin-top: 0;}
 h1.one:before {content: "";display: block;border-top: solid 1px black;width: 100%;height: 1px;
    position: absolute;top: 50%;z-index: 1;}
  h1.one span { background: #ffc; padding: 0 20px; position: relative; z-index: 5;}
  </style>
<script language="javascript" type="text/javascript">
function limitText(limitField, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    }
}
</script>
<script>
function checkLength(){
    var textbox = document.getElementById("textbox");
    if(textbox.value.length <= 6 && textbox.value.length >= 6){
        
    }
    else{
        alert("Charges Must be between 10000 and 29999 Ksh.!!")
    }
}
</script>
 <style>input.upc { text-transform: uppercase; }
</style>
<script>
            // thanks 2 'm2pc' : http://www.webdeveloper.com/forum/showpost.php?s=9f258cba84d461026bb9ed478e86776d&p=423545&postcount=3
            function doGetCaretPosition (oField) {
                var iCaretPos = 0;
                if (document.selection) // IE Support
                    {
                    oField.focus ();
                    var oSel = document.selection.createRange ();
                    // Move selection start to 0 position
                    oSel.moveStart ('character', -oField.value.length);
                    // The caret position is selection length
                    iCaretPos = oSel.text.length;
                    }
                else
                    if (oField.selectionStart || oField.selectionStart == '0') // Firefox support
                        iCaretPos = oField.selectionStart;
                return (iCaretPos);
                }
            function doSetCaretPosition (oField, iCaretPos)
                {
                if (document.selection) // IE Support
                    {
                    oField.focus ();
                    var oSel = document.selection.createRange ();
                    oSel.moveStart ('character', -oField.value.length);
                    oSel.moveStart ('character', iCaretPos);
                    oSel.moveEnd ('character', 0);
                    oSel.select ();
                    }
                else
                    if (oField.selectionStart || oField.selectionStart == '0') // Firefox support
                        {
                        oField.selectionStart = iCaretPos;
                        oField.selectionEnd = iCaretPos;
                        oField.focus ();
                        }
                }
            function forceupper(o)
                {
                var x = doGetCaretPosition(o);
                o.value=o.value.toUpperCase();
                doSetCaretPosition(o,x);
                }
        </script>
</head>
<body bgcolor="#F4FFE4">
<div id="hady"><h1 style="background: #2F4F4F; color:#4CAF50; margin-bottom:30px; font-family:Baskerville, 'Palatino Linotype', Palatino, 'Century Schoolbook L', 'Times New Roman', serif;" align="center"> <font size="+2">Online Machinery Rentals </font></h1>
</div>
<div id="logos">
</div><br /><br />
 <style>
#link ul{
	margin-left:400px;
	 }
   </style>
<div id="link">
<ul>
<li><a href="">Add Equipment</a></li>
<li><a href="edit.php">Manage Users</a></li>
<li><a href="view.php">View Orders</a></li>
<li><a href="report.php">Generate Reports</a></li>
</ul>
</div>

<h1 style="background: #2F4F4F; color:#4CAF50; margin-top:5px; font-family:Baskerville, 'Palatino Linotype', Palatino, 'Century Schoolbook L', 'Times New Roman', serif;" align="center"> <font size="+2">Add New Equipment |</font><a style="text-decoration:none;" href="logout.php"><font style="color:white; size:0% margin-left:100%; ">Logout</font></a></h1>
<div id = "form" style ="width:800px;" align="center" >
<form action="" method="post"  target="#">
<h1 class="one"><span><font face="grey">New Equipment</font></span></h1>
<label style="margin-left: -50px;">Category:</label>
<select id='selectgen' name='category' placeholder="Gender" style="width: 150px; margin-left: 50px; height: 25px;">
			<option disabled selected value='0'>.....Select Category.....</option>
			<option value='Cement mixing'>Cement mixing</option>
			<option value='Transport systems'>Transport systems</option>
			<option value='Excavating'>Excavating</option>
			<option value='Compaction'>Compaction</option>
			<option value='Drilling'>Drilling</option>
            <option value='Lifting'>Lifting</option>
			</select></br></br>
<label style="margin-left: -50px;">Equipment:</label>
<input type="text" name="equipment" placeholder="Equipment Name" id="name1" style="width: 150px; margin-left: 50px; height: 25px;"/></br></br>
<label style="margin-left: -30px;">Reg No:</label>
<!--<input name="f1" id="idf1" class="upc" type="text">
<input name="f2" id="idf2" class="js" onkeyup="forceupper(this);">-->
<input type="text" name="regno" placeholder="Reg No." class="js" onkeyup="forceupper(this);" id="idf2" style="width: 150px; margin-left: 50px; height: 25px;"/></br></br>
<label style="margin-left: -50px;">Price/Day:</label>
<input type="text" name="price" placeholder="Price in KShs" id="textbox" style="width: 150px; margin-left: 50px; height: 25px;" onKeyDown="limitText(this,6);" 
onKeyUp="limitText(this,6);"" /></br></br>
<input type="submit" name="submit"  onclick="checkLength()" value="ADD"/>
</form>
</body>
</html>
<?php
//session_start(); 
//Register a user
// Get values from form

if(!empty($_POST['category']) && !empty($_POST['equipment']) && !empty($_POST['regno']) && !empty($_POST['price']))
{
    $category = mysql_real_escape_string($_POST['category']);
    $equipment = mysql_real_escape_string($_POST['equipment']);
	$regno = mysql_real_escape_string($_POST['regno']);
	$price = mysql_real_escape_string($_POST['price']);
	
	$checkusername = mysql_query("SELECT * FROM equipments WHERE regno = '".$regno."'");
      
     if(mysql_num_rows($checkusername) == 1)
     {
	  echo'<script type="text/javascript">alert("Sorry, that equipment already exists. Please go back and try again.!");</script>';
        //echo "<h1>Error</h1>";
        //echo "<p>Sorry, that username is taken. Please go back and try again.</p>";
     }
     else
     {
        $registerquery = mysql_query("INSERT INTO equipments (category, eqname,regno, cost)
		VALUES('".$category."', '".$equipment."','".$regno."','".$price."')");
        if($registerquery)
        {
		 echo '<script>alert("New Equipment was successfully Added..!")</script>';
		 echo "<script>window.open('add.php','_self')</script>";
        }
        else 
        {
            echo '<script type="text/javascript">alert("Failed To Add. Please Try Again.!");</script>';	    
        }       
}
}
exit;

?>

